# dotfiles

Preferences & settings installer & backup.

Setup installs various packages like: git, Docker Desktop, Sublime Text 4, Node.js, Ruby, Python, ESLint, Stylelint, yamllint, markdownlint

## Setup on Windows 10

Open a "Run as Administrator" PowerShell window and run the following:

```bash
# Install git using winget (Windows Package Manager)
# winget is available automatically on Windows 10 version 1809 and later
winget install -e Git.Git
# Refresh PATH so packages are accessible without shell close/reopen
$env:Path = [System.Environment]::GetEnvironmentVariable('Path','Machine')

# git clone dotfiles repository directly into user's profile directory
cd $env:USERPROFILE
git init
git remote add origin https://gitlab.com/haayden/dotfiles.git
git fetch
git checkout main -f
git remote remove origin
git remote add origin git@gitlab.com:haayden/dotfiles.git
git push --set-upstream origin main

# Run setup PowerShell script to install many packages and symlinks
& "$env:USERPROFILE\.config\powershell\setup_windows.ps1"
```

## Useful Commands Reference

Get list of globally installed pip packages: `pip list`

Get list of globally installed npm packages: `npm list -g --depth 0`

Uninstall specific globally installed npm package: `npm uninstall -g stylelint`

Uninstall all npm global packages using PowerShell: `Remove-Item "$env:APPDATA\npm" -Recurse -Force`
