# Install packages using winget (Windows Package Manager)
# winget is available automatically on Windows 10 version 1809 and later
winget install Docker.DockerDesktop
winget install SublimeHQ.SublimeText.4
winget install OpenJS.NodeJS
winget install RubyInstallerTeam.Ruby.3.2
winget install Python.Python.3.11
# Refresh PATH so packages are accessible without shell close/reopen
$env:Path = [System.Environment]::GetEnvironmentVariable('Path','Machine')

# Globally install many linting packages
npm install --global postcss stylelint stylelint-config-standard stylelint-order markdownlint-cli
gem install rubocop rubocop-performance
pip install yamllint

# Install npm eslint packages globally so it's available on the PATH
npx install-peerdeps eslint-config-airbnb-base --global
# Also install npm eslint packages locally in UserProfile to fix issues extending from airbnb-base
cd $env:USERPROFILE
npx --yes install-peerdeps eslint-config-airbnb-base

# Allow local PowerShell scripts to run
Set-ExecutionPolicy RemoteSigned

# Symlink files and folders to point toward git repository .config versions
function H-Create-Symbolic-Link {
  # Remove file if it exists
  if (Test-Path $FilePath) { Remove-Item $FilePath }
  New-Item -ItemType SymbolicLink -Path $FilePath -Target $FileTarget
}
function H-Create-Folder-Link {
  # Remove folder if it exists
  if (Test-Path $FolderPath) { Remove-Item $FolderPath -Recurse -Force }
  New-Item -ItemType Junction -Path $FolderPath -Target $FolderTarget
}
$FilePath = "$env:USERPROFILE\.eslintrc.js"
$FileTarget = "$env:USERPROFILE\.config\eslint\.eslintrc.js"
H-Create-Symbolic-Link
$FilePath = "$env:USERPROFILE\.editorconfig"
$FileTarget = "$env:USERPROFILE\.config\editorconfig\.editorconfig"
H-Create-Symbolic-Link
$FilePath = "$env:USERPROFILE\.rubocop.yml"
$FileTarget = "$env:USERPROFILE\.config\rubocop\.rubocop.yml"
H-Create-Symbolic-Link
$FilePath = "$env:USERPROFILE\.markdownlintrc"
$FileTarget = "$env:USERPROFILE\.config\markdownlint\.markdownlintrc"
H-Create-Symbolic-Link
$FilePath = "$env:USERPROFILE\.stylelintrc.json"
$FileTarget = "$env:USERPROFILE\.config\stylelint\.stylelintrc.json"
H-Create-Symbolic-Link
New-Item -ItemType Directory -Force -Path "$env:USERPROFILE\Documents\WindowsPowerShell"
$FilePath = "$env:USERPROFILE\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1"
$FileTarget = "$env:USERPROFILE\.config\powershell\Microsoft.PowerShell_profile.ps1"
H-Create-Symbolic-Link
$FolderPath = "$env:APPDATA\Sublime Text\Packages\User"
$FolderTarget = "$env:USERPROFILE\.config\sublime_text\User"
H-Create-Folder-Link
